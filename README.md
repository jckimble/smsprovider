# smsprovider
[![GoDoc](https://godoc.org/gitlab.com/jckimble/smsprovider?status.svg)](https://godoc.org/gitlab.com/jckimble/smsprovider)
[![pipeline status](https://gitlab.com/jckimble/smsprovider/badges/master/pipeline.svg)](https://gitlab.com/jckimble/smsprovider/commits/master)
[![coverage report](https://gitlab.com/jckimble/smsprovider/badges/master/coverage.svg)](https://gitlab.com/jckimble/smsprovider/commits/master)

Package `jckimble/smsprovider` implements providers for sms services. Made to make sms bots/gateways easier. Pull Requests welcome for new providers that you like.

---
* [Install](#install)
* [Providers](#providers)
* [Examples](#examples)

---

## Install
```sh
go get -u gitlab.com/jckimble/smsprovider
```

## Providers
 - TextNow
 - Signal

## Examples
Look under the examples directory right now we have two examples. A bot and gateway

## License

Copyright 2018 James Kimble

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
